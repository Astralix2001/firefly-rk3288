/*
 * Copyright (C) 2010 The Android Open Source Project
 * Copyright (C) 2012-2013, The Linux Foundation. All rights reserved.
 *
 * Not a Contribution, Apache license notifications and license are
 * retained for attribution purposes only.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cutils/properties.h>
#include <utils/Log.h>
#include <utils/Timers.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <poll.h>
#include <ui/PixelFormat.h>
#include "hwc.h"
#include "../libgralloc_ump/gralloc_priv.h"
#include "../libon2/vpu_global.h"

#define RK_FBIOSET_CONFIG_DONE			0x4628
#define RK_FBIOSET_YUV_ADDR				0x5002
#define RK_FBIOSET_OVERLAY_STATE     	0x5018
#define RK_FBIOSET_ENABLE				0x5019

#define HDMI_3D_NONE  -1
#define HDMI_3D_FRAME_PACKING 0
#define HDMI_3D_TOP_BOTTOM 6
#define HDMI_3D_SIDE_BY_SIDE_HALT 8

void dump_fps(void) {
	
	char property[PROPERTY_VALUE_MAX];
	if (property_get("debug.hwc.logfps", property, "0") && atoi(property) > 0) {
		static int mFrameCount;
	    static int mLastFrameCount = 0;
	    static nsecs_t mLastFpsTime = 0;
	    static float mFps = 0;
	    mFrameCount++;
	    nsecs_t now = systemTime();
	    nsecs_t diff = now - mLastFpsTime;
	    if (diff > ms2ns(500)) {
	        mFps =  ((mFrameCount - mLastFrameCount) * float(s2ns(1))) / diff;
	        mLastFpsTime = now;
	        mLastFrameCount = mFrameCount;
	        ALOGD("---mFps = %2.3f", mFps);
	    }
	    // XXX: mFPS has the value we want
	}
}

void hwc_dump_layer(hwc_display_contents_1_t* list)
{
    size_t i;
    static int DumpSurfaceCount = 0;

    char pro_value[PROPERTY_VALUE_MAX];
    property_get("sys.dump",pro_value,0);
    //LOGI(" sys.dump value :%s",pro_value);
    if(!strcmp(pro_value,"true"))
    {
        for (i = 0; list && (i < (list->numHwLayers - 1)); i++)
        {
            hwc_layer_1_t const * l = &list->hwLayers[i];

            if(l->flags & HWC_SKIP_LAYER)
            {
                ALOGI("layer %p skipped", l);
            }
            else
            {
                struct private_handle_t * handle_pre = (struct private_handle_t *) l->handle;
                int32_t SrcStride ;
                FILE * pfile = NULL;
                char layername[100] ;


                if( handle_pre == NULL)
                    continue;

                SrcStride = android::bytesPerPixel(handle_pre->format);
                memset(layername,0,sizeof(layername));
                system("mkdir /data/dump/ && chmod /data/dump/ 777 ");
                //mkdir( "/data/dump/",777);
                sprintf(layername,"/data/dump/dmlayer%d_%d_%d_%d.bin",DumpSurfaceCount,handle_pre->stride,handle_pre->height,SrcStride);
                DumpSurfaceCount ++;
                pfile = fopen(layername,"wb");
                if(pfile)
                {
                    fwrite((const void *)handle_pre->base,(size_t)(SrcStride * handle_pre->stride*handle_pre->height),1,pfile);
                    fclose(pfile);
                    ALOGI(" dump surface layername %s,w:%d,h:%d,formatsize :%d",layername,handle_pre->width,handle_pre->height,SrcStride);
                }
            }
        }

    }
    property_set("sys.dump","false");
}

int openFramebufferDevice(hwc_context_t *ctx)
{
	struct fb_fix_screeninfo finfo;
    struct fb_var_screeninfo info;

    int fb_fd = open("/dev/graphics/fb0", O_RDWR, 0);
	
	if(fb_fd < 0) {
		ALOGE("/dev/graphics/fb0 cat not access.");
		return -1;
	}
	
    if (ioctl(fb_fd, FBIOGET_VSCREENINFO, &info) == -1)
        return -errno;

	property_set("sys.use.3dmode","-1");
	
    if (int(info.width) <= 0 || int(info.height) <= 0) {
        // the driver doesn't return that information
        // default to 160 dpi
        info.width  = ((info.xres * 25.4f)/160.0f + 0.5f);
        info.height = ((info.yres * 25.4f)/160.0f + 0.5f);
    }

    float xdpi = (info.xres * 25.4f) / info.width;
    float ydpi = (info.yres * 25.4f) / info.height;

    if (ioctl(fb_fd, FBIOGET_FSCREENINFO, &finfo) == -1)
        return -errno;

    if (finfo.smem_len <= 0)
        return -errno;

	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].hdmi3d_mode = -1;
    ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].fd = fb_fd;
    ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].active = true;
    //xres, yres may not be 32 aligned
    ctx->dpyAttr[HWC_DISPLAY_PRIMARY].stride = finfo.line_length;
    ctx->dpyAttr[HWC_DISPLAY_PRIMARY].xres = info.xres;
    ctx->dpyAttr[HWC_DISPLAY_PRIMARY].yres = info.yres;
    ctx->dpyAttr[HWC_DISPLAY_PRIMARY].xdpi = xdpi;
    ctx->dpyAttr[HWC_DISPLAY_PRIMARY].ydpi = ydpi;
    ctx->dpyAttr[HWC_DISPLAY_PRIMARY].vsync_period = 1000000000l / 60;

    ctx->dpyAttr[HWC_DISPLAY_PRIMARY].isActive = true;
    
    ctx->mCopyBit = new CopyBit();
    
	//Enable overlay mode
	char property[PROPERTY_VALUE_MAX];
	int overlay;
	if (property_get("video.use.overlay", property, "0") && atoi(property) > 0) {
		fb_fd = open("/dev/graphics/fb1", O_RDWR, 0);
		if(fb_fd < 0) {
			ALOGE("/dev/graphics/fb0 cat not access.");
			return -1;
		}
		else
			ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[1].fd = fb_fd;
		
		hwc_enable_layer(ctx, HWC_DISPLAY_PRIMARY, 1, 0);
		
		overlay = 1;
		ioctl(ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].fd, RK_FBIOSET_OVERLAY_STATE, &overlay);
		// If sys.ui.fakesize is not defined, default set to 1280x720
		if(property_get("sys.ui.fakesize", property, NULL) <= 0) {
			ALOGD("set default fake ui size 1280x720");
			property_set("sys.ui.fakesize", "1280x720");
		}
		property_set("sys.hwc.compose_policy", "0");
	}
	else {
		property_set("sys.yuv.rgb.format", "1");
		overlay = 0;
		ioctl(ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].fd, RK_FBIOSET_OVERLAY_STATE, &overlay);
	}
	
//	if (ioctl(ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].fd, RK_FBIOSET_CONFIG_DONE, &info) == -1)
//		return -errno;
		
    return 0;
}

static unsigned int videodata[2];

void hwc_set_hdmi_mode(hwc_context_t *ctx, int NewFrameRate)
{
	int msize = 64;
	char szLine[msize];
	int fps = 0;
	char* temp = NULL;
	char test[msize];
	char w[16],h[16],hz[16];
	char curw[16],curh[16],curhz[16];
	FILE* fp0 = NULL;
	int mret = -1;
	
	char property[PROPERTY_VALUE_MAX];
	property_get("persist.sys.hwc.audohdmimode", property, 0);
	//ALOGD("\033[0;31m property = %d \033[0m\n",atoi(property));;
	if(atoi(property) == 0)
		return;
	
	if((fp0 = fopen("/sys/class/display/display0.HDMI/mode","r")) == NULL)
			return;
	
	fgets(szLine,sizeof(szLine),fp0);
	sscanf(szLine,"%[^a-zA-Z]%*[a-zA-Z]%[^a-zA-Z]%*[a-z]-%[0-9]",curw,curh,curhz);
	ALOGD("curw = %s curh = %s curhz = %s",curw,curh,curhz);
	fclose(fp0);
	
	if(ctx->isModechange && NewFrameRate == 0)
	{
		// Restore original hdmi mode
		ctx->isModechange = false;
		mret = open("/sys/class/display/display0.HDMI/mode",O_RDWR);
		if(strlen(ctx->mCurmode))
			write(mret,ctx->mCurmode,sizeof(ctx->mCurmode));
		close(mret);
		return;
	}	
	
	if(NewFrameRate != atoi(curhz))
	{
		// Set HDMI frame rate equal to current video frame rate
		FILE* fp = NULL;
		if((fp = fopen("/sys/class/display/display0.HDMI/modes","r")) == NULL)
			return;
		while(fgets(szLine,sizeof(szLine),fp) != NULL)
		{
			sscanf(szLine,"%[^a-zA-Z]%*[a-zA-Z]%[^a-zA-Z]%*[a-z]-%[0-9]",w,h,hz);
			if((atoi(w) == atoi(curw)) && (atoi(h) == atoi(curh)) && (atoi(hz)==NewFrameRate))
			{
				ctx->isModechange =  true;
				ALOGD("(atoi(w) == atoi(curw)) strlen szline = %d DecodeFrmNum =  %d",strlen(szLine),NewFrameRate);
				mret = open("/sys/class/display/display0.HDMI/mode",O_RDWR);
				write(mret,szLine,sizeof(szLine));
				close(mret);
			}
		}
		fclose(fp);
	}
}

static void hwc_save_hdmi_mode(hwc_context_t *ctx)
{
	ctx->isModechange = false;
	FILE* fp0 = NULL;
	char szLine[32];
	fp0 = fopen("/sys/class/display/display0.HDMI/mode","r");
	fgets(szLine,sizeof(szLine),fp0);
	fclose(fp0);
	sprintf(ctx->mCurmode,"%s",szLine);
}

int hwc_overlay(hwc_context_t *ctx, int dpy, hwc_layer_1_t *Src)
{	
	struct private_handle_t* srchnd = (struct private_handle_t *) Src->handle;
	struct tVPU_FRAME *pFrame  = NULL;	
	hwc_rect_t * DstRect = &(Src->displayFrame);
	int enable, nonstd, grayscale;
	
	ALOGD_IF(HWC_DEBUG, "%s format %x width %d height %d address 0x%x", __FUNCTION__, srchnd->format, srchnd->width, srchnd->height, srchnd->base);

	if(srchnd->format == HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO || srchnd->format == 0x22) {
		pFrame = (tVPU_FRAME *)srchnd->base;
		ALOGD_IF(HWC_DEBUG, "%s video Frame addr=%x,FrameWidth=%d,FrameHeight=%d DisplayWidth=%d, DisplayHeight=%d, FrameRate=%d",
		 __FUNCTION__, pFrame->FrameBusAddr[0], pFrame->FrameWidth, pFrame->FrameHeight, pFrame->DisplayWidth, pFrame->DisplayHeight, pFrame->DecodeFrmNum);
	}
	else
		return 0;
	
	ALOGD_IF(HWC_DEBUG, "%s DstRect %d %d %d %d", __FUNCTION__, DstRect->left, DstRect->top, DstRect->right, DstRect->bottom);
	
	if(ctx->dpyAttr[dpy].layer[1].active == false) {
		if(hwc_enable_layer(ctx, dpy, 1, 1))
			return -1;
		videodata[0] = 0;
		videodata[1] = 0;
		hwc_save_hdmi_mode(ctx);
	}

	if(pFrame->DecodeFrmNum == 23)
		pFrame->DecodeFrmNum = 24;
	else if(pFrame->DecodeFrmNum == 29)
		pFrame->DecodeFrmNum = 30;
	else if(pFrame->DecodeFrmNum == 59)
		pFrame->DecodeFrmNum = 60;
	hwc_set_hdmi_mode(ctx, pFrame->DecodeFrmNum);

	struct fb_var_screeninfo info;
	
	if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, FBIOGET_VSCREENINFO, &info) == -1)
	{
		ALOGE("%s(%d):  fd[%d] Failed", __FUNCTION__, __LINE__, ctx->dpyAttr[dpy].layer[1].fd);
        	return -1;
	}
	nonstd = (DstRect->left<<8) + (DstRect->top<<20);
	if(ctx->dpyAttr[dpy].layer[0].hdmi3d_mode == HDMI_3D_SIDE_BY_SIDE_HALT && ((DstRect->right*2) <= ctx->dpyAttr[dpy].xres)){
		grayscale = ((DstRect->right*2 - DstRect->left) << 8) + ((DstRect->bottom - DstRect->top) << 20);
	}else if(ctx->dpyAttr[dpy].layer[0].hdmi3d_mode == HDMI_3D_TOP_BOTTOM && ((DstRect->bottom*2) <= ctx->dpyAttr[dpy].yres)){	
		grayscale = ((DstRect->right - DstRect->left) << 8) + ((DstRect->bottom*2 - DstRect->top) << 20);
	}else{
		grayscale = ((DstRect->right - DstRect->left) << 8) + ((DstRect->bottom - DstRect->top) << 20);
	}
	
	info.activate = FB_ACTIVATE_NOW;
	if(videodata[0] && (nonstd != (info.nonstd & 0xffffff00) || grayscale != (info.grayscale & 0xffffff00)) ) {
		info.activate |= FB_ACTIVATE_FORCE;
		ALOGD("active on now");
	}
	else
		info.activate |= FB_ACTIVATE_VBL;
		
	info.nonstd &= 0x00;
	if(srchnd->format == 0x22 || pFrame->OutputWidth==0x22)
		info.nonstd |= 0x22;
	else
		info.nonstd |= HAL_PIXEL_FORMAT_YCrCb_NV12;		
	info.nonstd |= nonstd;
	info.grayscale &= 0xff;
	info.grayscale |= grayscale;
	
	info.xoffset = 0;
	info.yoffset = 0;
	info.xres = pFrame->DisplayWidth;
	info.yres = pFrame->DisplayHeight;
	info.xres_virtual = pFrame->FrameWidth;
	info.yres_virtual = pFrame->FrameHeight;
//	info.rotate = ;
	/* Check yuv format. */
	if(videodata[0] != pFrame->FrameBusAddr[0]) {
		videodata[0] = pFrame->FrameBusAddr[0];
		videodata[1] = pFrame->FrameBusAddr[1];
//		if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, RK_FBIOSET_YUV_ADDR, videodata) == -1)
//		{	
//	    		ALOGE("%s(%d):  fd[%d] Failed,DataAddr=%x", __FUNCTION__, __LINE__,ctx->dpyAttr[dpy].layer[1].fd,videodata[0]);	
//	    		return -errno;
//		}
	}
	
	if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, FBIOPUT_VSCREENINFO, &info) == -1)
	    return -errno;
	    
//	if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, RK_FBIOSET_CONFIG_DONE, NULL) == -1)
//		return -errno;
	return 0;
}

int hwc_postfb(hwc_context_t *ctx, int dpy, hwc_layer_1_t *Src)
{
	struct private_handle_t* srchnd = (struct private_handle_t *) Src->handle;	
	hwc_rect_t * DstRect = &(Src->displayFrame);
	
	if(dpy == 0 && srchnd) {
		struct _rga_img_info_t src, dst;
		memset(&src, 0, sizeof(struct _rga_img_info_t));
		memset(&dst, 0, sizeof(struct _rga_img_info_t));
		
		src.yrgb_addr = (uint32_t)srchnd->base;
		src.uv_addr = 0;
		src.v_addr = 0;
		src.vir_w = srchnd->width;
		src.vir_h = srchnd->height;
		src.format = RK_FORMAT_RGBA_8888;
		if(ctx->dpyAttr[dpy].layer[0].hdmi3d_mode == HDMI_3D_SIDE_BY_SIDE_HALT){
			src.act_w = srchnd->width/2;
			src.act_h = srchnd->height;

			dst.act_w = srchnd->width/2;
			dst.act_h = srchnd->height;
			dst.x_offset = srchnd->width/2;
			dst.y_offset = 0;
		}else if(ctx->dpyAttr[dpy].layer[0].hdmi3d_mode == HDMI_3D_TOP_BOTTOM){
			src.act_w = srchnd->width;
			src.act_h = srchnd->height/2;

			dst.act_w = srchnd->width;
			dst.act_h = srchnd->height/2;
			dst.x_offset = 0;
			dst.y_offset = srchnd->height/2;
		}
		src.x_offset = 0;
		src.y_offset = 0;

		dst.yrgb_addr = (uint32_t)srchnd->base;
		dst.uv_addr  = 0;
		dst.v_addr	 = 0;
		dst.format = RK_FORMAT_RGBA_8888;
		dst.vir_w = ((srchnd->width*2 + (8-1)) & ~(8-1))/2;		
		dst.vir_h = srchnd->height;
		if(ctx->dpyAttr[dpy].layer[0].hdmi3d_mode == HDMI_3D_SIDE_BY_SIDE_HALT || ctx->dpyAttr[dpy].layer[0].hdmi3d_mode == HDMI_3D_TOP_BOTTOM)
			ctx->mCopyBit->draw(&src,&dst,RK_MMU_SRC_ENABLE | RK_MMU_DST_ENABLE);
		
		ALOGD_IF(HWC_DEBUG, "%s format %x width %d height %d address 0x%x offset 0x%x", __FUNCTION__, srchnd->format, srchnd->width, srchnd->height, srchnd->base, srchnd->offset);
		
		struct fb_var_screeninfo info;
		if (ioctl(ctx->dpyAttr[dpy].layer[0].fd, FBIOGET_VSCREENINFO, &info) == -1)
	    		return -errno;
	    	info.yoffset = srchnd->offset/ctx->dpyAttr[dpy].stride;
		if (ioctl(ctx->dpyAttr[dpy].layer[0].fd, FBIOPUT_VSCREENINFO, &info) == -1)
			return -errno;
//		if (ioctl(ctx->dpyAttr[dpy].layer[0].fd, RK_FBIOSET_CONFIG_DONE, NULL) == -1)
//			return -errno;
	}

	return 0;
}

int hwc_yuv2rgb(hwc_context_t *ctx, hwc_layer_1_t *Src)
{
	struct private_handle_t* srchnd = (struct private_handle_t *) Src->handle;
	
	if(ctx->mCopyBit == NULL) {
		ALOGE("%s device not ready.", __FUNCTION__);
		return -1;
	}
	
	if(srchnd == NULL || srchnd->format != HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO) {
		ALOGE("%s no need to do color convert.", __FUNCTION__);
		return 0;
	}
	
	struct tVPU_FRAME *pFrame  = (tVPU_FRAME *)srchnd->base;
	ALOGD_IF(HWC_DEBUG, "%s video Frame addr=%x,FrameWidth=%u,FrameHeight=%u DisplayWidth=%u, DisplayHeight=%u",
	 __FUNCTION__, pFrame->FrameBusAddr[0], pFrame->FrameWidth, pFrame->FrameHeight, pFrame->DisplayWidth, pFrame->DisplayHeight);
	
	if(pFrame->FrameWidth > 3840 || pFrame->FrameHeight > 2160 || pFrame->FrameBusAddr[0] == 0xFFFFFFFF) {
		ALOGE("%s error parameter, cannot convert.", __FUNCTION__);
		return -1;
	}
	
//	memset((void*)srchnd->base, 0xFF, srchnd->width * srchnd->height * 4);
//	return 0;
	struct _rga_img_info_t src, dst;
	memset(&src, 0, sizeof(struct _rga_img_info_t));
	memset(&dst, 0, sizeof(struct _rga_img_info_t));

#ifdef TARGET_RK32
	src.yrgb_addr =	(int)pFrame->FrameBusAddr[0]; 
	dst.format = RK_FORMAT_YCbCr_420_SP;
#else
	src.yrgb_addr =	(int)pFrame->FrameBusAddr[0]+ 0x60000000;
	dst.format = RK_FORMAT_RGBA_8888;
#endif
	src.uv_addr	= src.yrgb_addr + ((pFrame->FrameWidth + 15)&(~15)) * ((pFrame->FrameHeight+ 15)&(~15));
	src.v_addr   = src.uv_addr;
    src.vir_w = (pFrame->FrameWidth + 15)&(~15);
    src.vir_h = (pFrame->FrameHeight + 15)&(~15);
    src.format = RK_FORMAT_YCbCr_420_SP;
  	src.act_w = pFrame->DisplayWidth;
    src.act_h = pFrame->DisplayHeight;
    src.x_offset = 0;
    src.y_offset = 0;
	

	dst.yrgb_addr = (uint32_t)srchnd->base;
	dst.uv_addr  = 0;
	dst.v_addr   = 0;	
   	dst.vir_w = ((srchnd->width*2 + (8-1)) & ~(8-1))/2;
    dst.vir_h = srchnd->height;
	dst.act_w = pFrame->DisplayWidth;
	dst.act_h = pFrame->DisplayHeight;
	dst.x_offset = 0;
	dst.y_offset = 0;
#ifndef TARGET_RK32
	return ctx->mCopyBit->draw(&src, &dst, RK_MMU_SRC_ENABLE | RK_MMU_DST_ENABLE | RK_BT_601_MPEG);
#else
	return ctx->mCopyBit->draw(&src, &dst, RK_MMU_DST_ENABLE | RK_BT_601_MPEG);
#endif
	
}

int hwc_enable_layer(hwc_context_t *ctx, int dpy, int layer, int active)
{
	if (ioctl(ctx->dpyAttr[dpy].layer[layer].fd, RK_FBIOSET_ENABLE, &active) == -1)
		return -errno;
	
//	if (ioctl(ctx->dpyAttr[dpy].layer[layer].fd, RK_FBIOSET_CONFIG_DONE, NULL) == -1)
//		return -errno;
	
	ctx->dpyAttr[dpy].layer[layer].active = active;
	
	return 0;
}
